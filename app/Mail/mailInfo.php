<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailInfo extends Mailable
{
    use Queueable, SerializesModels;

    protected $mail;

    protected $mensaje;
    protected $asunto;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($_mail, $_mensaje, $_asunto) {
        $this->mail = $_mail;
        $this->mensaje = $_mensaje;
        $this->asunto = $_asunto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $data = [
            'mail' => $this->mail,
            'mensaje' => $this->mensaje,
        ];
        return $this->view('mail.mailinfo')->with('data', $data)->from($this->mail, $this->mail)->subject($this->asunto);

    }
}
