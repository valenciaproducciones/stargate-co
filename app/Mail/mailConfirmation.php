<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $nombre;

    protected $mensaje;
    protected $asunto;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($_nombre, $_mensaje, $_asunto) {
        $this->nombre = $_nombre;
        $this->mensaje = $_mensaje;
        $this->asunto = $_asunto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $data = [
            'nombre' => $this->nombre,
            'mensaje' => $this->mensaje,
        ];
        return $this->view('mail.mail')->with('data', $data)->from('info@stargatestudios.co', 'Stargatestudios Colombia')->subject($this->asunto);
    }
}
