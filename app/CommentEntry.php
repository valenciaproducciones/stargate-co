<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentEntry extends Model {
    protected $fillable = [
        'nombre',
        'telefono',
        'email',
		'comentario'
    ];

    protected $table = 'comentarios';
}
