<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\CommentEntry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use App\Mail\mailConfirmation;
use App\Mail\mailInfo;
use Mail;

class StargateController extends Controller
{
	public function AddComment(Request $request) {
        $data = $request->json()->all();
        $validatorRules = [
            'nombre' => 'required|string',
            'telefono' => 'required|string',
            'email' => 'required|string',
            'comentario' => 'required|string',
           
        ];
        $validator = Validator::make($data, $validatorRules);
		
		
		
		
        if (!$validator->fails()) {
			
			$keepSaving = true;
			
			
			if($keepSaving)
			{
				$entry = new CommentEntry ;
				$entry->nombre = isset($data['nombre']) ? $data['nombre'] : '';
				$entry->telefono = isset($data['telefono']) ? $data['telefono'] : '';
				$entry->email = isset($data['email']) ? $data['email'] : '';
				$entry->comentario = isset($data['comentario']) ? $data['comentario'] : '';
				
				$entry->save();

				$result = [
					'status' => 'OK',
				];

				
				
					$message = "<p>Muchas gracias por comunicarte con Stargate Studios Colombia.<br>Pronto nos comunicaremos contigo.</p>";
					Mail::to($entry->email)->send(new mailConfirmation($entry->nombre, $message, 'Contácto Stargate Studios Colombia'));
					Mail::to('info@stargatestudios.co')->send(new mailInfo($entry->email, ('Comentario: '.$entry->comentario.'<br><br>E-mail: '.$entry->email.'<br>Teléfono: '.$entry->telefono.'<br>Nombre:'.$entry->nombre), 'Comentario Stargate'));
			
				return $result;
			}
        }

        $result = [
            'status' => 'Error',
            'errors' => $validator->errors()
        ];

        return $result;
    }
}
