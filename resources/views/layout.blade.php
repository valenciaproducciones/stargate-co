<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>      
	<title>Stargate Studios Colombia</title>
	 <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('icon/i.png') }}">
     
	<meta name="viewport" content="width=device-width, initial-scale=1">	
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N2948PS');</script>
<!-- End Google Tag Manager -->
			
		<link rel="stylesheet" href="{{ mix('css/app.css') }}?v={{ env('APP_VERSION', '0') }}" />
		@yield('styles')
		
		
	
	</head>
	<body class="" >
		
	
		<div  id="app" class="flex-center position-ref full-height">
			
				<video id="inicio" class="reel-banner" autoplay loop muted>
				
				 <source src="/assets/reel.mp4" type="video/mp4">
				</video>
				
				<div class="col-12 col-md-12 text-center overlay">
					<a href="/">
						<img class="my-4" src="/assets/logo.png" style="height:40vh; max-width:100%"/></a>
				</div>
				<div class="row menu justify-content-center pt-3">	
                    <div class = "barMenu"></div>
					<br>
                    <div class = "col-1 col-md-1 menuitem text-center "><a href="/"><img class="logoMenu" src="/assets/logo-small.png" /></a></div>
                    <div class = "col-2 col-md-1 menuitem text-center"><a href="#inicio">Inicio</a></div>
                    <div class = "col-2 col-md-1 menuitem text-center"><a href="#galeria">Galería</a></div>
                    <div class = "col-2 col-md-1 menuitem text-center"><a href="#empresa">Empresa </a></div>
                    <!--div class = "col-12 col-md-1 menuitem text-center"><a href="/capitulos">Servicios </a></div>
                    <div class = "col-12 col-md-1 menuitem text-center"><a href="/capitulos">Empresa </a></div-->
                    <div class = "col-4 col-md-1 menuitem text-center"><a href="#contacto">Contáctenos</a></div>
                </div>
				
			<div id = "galeria" class="row justify-content-center orange-line py-4 px-4">
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/1.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/2.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/3.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/4.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/5.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/6.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/7.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/8.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/9.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/10.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/11.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/12.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/13.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/14.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/15.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/16.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/17.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/18.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/19.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/20.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/21.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/22.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/23.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/24.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/25.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/26.png" style="height:auto; max-width:100%"/></a></div>
					<div class = "col-md-2 px-0"><img class="my-auto" src="/assets/shows/27.png" style="height:auto; max-width:100%"/></a></div>
			</div>
				
			<div id="empresa" class="container vCenter" style="min-height:100vh">
			<div class="row">
			<div class="col-md-6">
			<p class="text-justify" style="font-size:1.2rem;  line-height: 1.5rem;">
			Stargate studios la galardonada compañía Hollywoodense de producción especializada en producción virtual y efectos visuales realizadora de las aclamadas producciones como: THE WALKING DEAD, CSI, SPARTACUS y 24 entre otras, se ha asociado con Valencia producciones FX para abrir Stargate Studios Colombia y servir a la industria del cine y la televisión en Colombia y Latinoamérica‼

Por medio de nuevas tecnologías, estrategias consolidadas de producción y nuevas  herramientas en previsualización, producción, producción virtual, efectos visuales, librería virtual blacklot y editorial optimizara los recursos y expandirá los imaginarios hacia la creación de nuevos proyectos audiovisuales al nivel de las grandes industrias del mundo.
			</p>
			</div>
			<div class="col-md-6 my-auto">
			<img class="my-auto" src="/assets/foto.jpg" style="height:auto; max-width:100%"/>
			</div>
			</div>
			</div>
			
				
            <div id="contacto" class="container text-justify mt-5 " >
				<h3>CONTACTO</h3>
				<div class="row orange-line py-4">
					<div class = "col-md-3">
						
						<span class="orange-text">BOGOTÁ</span><br>
						Carrera 22 #142-58<br>Bogotá, Colombia<br>
						<a href="tel:+573224576909"><span class="orange-text">+57</span>3224576909<br></a>
						<a href="mailto:info@stargatestudios.co">info@stargatestudios.co<br>
						<span class="orange-text"><a href="http://stargatestudios.co">stargatestudios.co</a></span>
						</p>
						<div class="social mb-2">
							<a target="_blank" href="https://www.facebook.com/stargatestudioscol" rel="" title="Síguenos en Facebook"><i class="fab fa-facebook-f"></i></a>
							<a target="_blank" href="https://twitter.com/stargatecol" rel="" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
							<a target="_blank" href="https://www.youtube.com/user/stargatestudioscol" rel="" title="Síguenos en Youtube"><i class="fab fa-youtube"></i></a>
							<a target="_blank" href="https://www.instagram.com/stargatestudioscol/" rel="" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
						</div>
					</div>
					
					<div class = "col-md-3">
						
						<span class="orange-text">LOS ANGELES</span><br>
						1001 El Centro St<br>South Pasadena, CA 91030<br>
						<a href="tel:+16264038403"><span class="orange-text">+1</span> 6264038403<br><a/>
						<a href="mailto:info+los-angeles@stargatestudios.net">info+los-angeles@stargatestudios.net</a><br>
						<span class="orange-text"><a target="_blank" href="http://stargatestudios.net/los-angeles-studio/">stargatestudios.net/los-angeles-studio/</a></span>
						</p>
					</div>
					
					<div class = "col-md-3">
						
						<span class="orange-text">ATLANTA</span><br>
						1431 Woodmont Ln NW<br>Atlanta, GA 30318<br>
						<a href="tel:+14049903799"><span class="orange-text">+1</span>404 990 3799<br></a>
						 <a href="mailto:info+atlanta@stargatestudios.net">info+atlanta@stargatestudios.net</a><br>
						<span class="orange-text"><a  target="_blank" href="http://stargatestudios.net/atlanta-studio/">stargatestudios.net/atlanta-studio</a></span>
						</p>
					</div>
					<div class = "col-md-3">
						
						<span class="orange-text">MALTA</span><br>
						2 Piazzetta Aparts, Triq l-Arzella <br>Bahar ic-Caghaq, NXR 5192, MALTA<br>
						<a href="tel:+35621371789"><span class="orange-text">+356</span> 21371 789<br></a>
						<a href="mailto:info+malta@stargatestudios.net">info+malta@stargatestudios.net</a><br>
						<span class="orange-text"><a target="_blank" href="http://stargatestudios.com.mt">stargatestudios.com.mt</a></span>
						</p>
					</div>
					<div class = "col-md-6">
						<contact-form></contact-form>
					</div>
				</div>
				
				<!--div class="row orange-line py-4 justify-content-center align-items-center">
					<div class = "col-md-2">
						Apoya:
					</div>
					<div class = "col-md-3">
						<img class="my-4" src="/assets/logos/gobierno.png" style="max-width:90%"/>
					</div>
					<div class = "col-md-3">
						<img class="my-4" src="/assets/logos/innpulsa.png" style="max-width:80%"/>
					</div>
					<div class = "col-md-2">
						<img class="my-4" src="/assets/logos/crea.png" style="max-width:60%"/>
					</div>
				</div-->
				
				<p class="foot orange-line py-4 text-center">Stargate Studios Colombia | Copyright 2019 Todos los derechos reservados | info@stargatestudios.co | www.stargatestudios.co | Términos y Condiciones | Cr 22#142-58. Bogotá,</p>
				
				@yield('content')
				
			</div>
		 </div>	
		<br>
		<div class= "row text-center">
		<div class="col-12 col-md-4 " style="font-size:1.2rem;">
		<div class="fb-like my-2" data-href="https://www.facebook.com/stargatestudioscol/" data-width="200" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="true">
		</div>
		</div>
		</div>
		
		
		
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2948PS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

     
		<div id="fb-root"></div>
		<script src="{{ mix('js/app.js') }}?v={{ env('APP_VERSION', '0') }}" type="text/javascript"></script>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0"></script>
		<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
		<script>
		  
		   $('.logoMenu').fadeOut(0);
		   $('.barMenu').fadeOut(0);
		  $(document).scroll(function() {
			  var y = $(this).scrollTop();
			 
			  if (y > 400) {
				$('.logoMenu').fadeIn();
				$('.barMenu').fadeIn();
			  } else {
				$('.logoMenu').fadeOut();
				$('.barMenu').fadeOut();
			  }
			});
		 
		</script>
		@yield('scripts')
	</body>
</html>