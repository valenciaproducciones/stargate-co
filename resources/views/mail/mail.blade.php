<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <div style="height:100%;margin:0;padding:0;width:100%">
        <center>
             <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
				<tbody>
					<tr>
						<td align="center" valign="top" style="background-color:#000000">
						  <br>
						  <img align="center" alt="" src="http://stargatestudios.co/assets/logo.png" width="" style="max-width:300px;padding-bottom:10;padding-top:10;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none">
						  <br>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top"  style="background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:0px;padding-bottom:0px">
							 <br>
							 <span style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">Hola {{ $data['nombre'] }}</span> 
							 <br><br> {!! $data['mensaje'] !!}
						</td>
					</tr>
				</tbody>
			</table>
        </center>
    </div>
</body>
</html>